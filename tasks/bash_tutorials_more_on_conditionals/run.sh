#!/bin/bash
read a
read b
read c

if [[ $a == $b && $a == $c ]]; then
    printf "EQUILATERAL"
else
    if [[ $a == $b || $a == $c || $b == $c ]]; then
        printf "ISOSCELES"
    else
        printf "SCALENE"
    fi
fi