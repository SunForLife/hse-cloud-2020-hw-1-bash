#!/bin/bash
awk '{ sum = $2 + $3 + $4; printf $0 " : "; if(sum >= 240) print "A"; else if(sum >= 180) print "B"; else if(sum >= 150) print "C"; else print "FAIL"}'