#!/bin/bash
read n

sum=0

for i in $(seq 1 $n);
    do
        read num
        sum=$((sum+num))
    done

echo "$sum/$n" | bc -l | xargs printf "%.3f\n"